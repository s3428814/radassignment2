class SessionsController < ApplicationController
  def new
  end
  
  def create
    users = Users.find_by(id: params[:session][:username].downcase)
    if users && users.authenticate((params[:session][:password]))
      log_in users
      redirect_to "/"
    else
      flash[:danger] = 'Invalid id/password'
      redirect_to '/login'
    end
  end
  
  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end
end
