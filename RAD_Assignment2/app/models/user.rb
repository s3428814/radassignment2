class User < ActiveRecord::Base
  attr_accessor :password
  has_secure_password
  
    PASSWORD_FORMAT = /\A
    (?=.{10,})          # Must contain 10 or more characters
    (?=.*\d)           # Must contain a digit
    (?=.*[a-z])        # Must contain a lower case character
    (?=.*[A-Z])        # Must contain an upper case character
    (?=.*[[:^alnum:]]) # Must contain a symbol
    /x
    
    #formatting for password

    USERNAME_FORMAT = /\A[a-z0-9A-Z\-_]{2,15}\z/ #Can contain lowercase and upercase letters, numbers, - and _, must be between 2 and 15 length
    
    #username formatting
    
    validates :username,
    :presence => true,
    :uniqueness => true,
    :format => USERNAME_FORMAT
    
    validates :password, 
    :presence => true, 
    :format => PASSWORD_FORMAT,
    :confirmation => true, 
    :on  => create 
    
    before_save :encrypt_password
    # before callback after the user object gets changed, so password is encrypted
    after_save :clear_password
    # after callback after the user object gets changed, so the plaintext password is cleared
    
    def encrypt_password
        if password.present?
            self.salt = BCrypt::Engine.generate_salt
            self.encrypted_password= BCrypt::Engine.hash_secret(password, salt)
        end
    end
    
    def clear_password
     self.password = nil
    end
    

end