Rails.application.routes.draw do

  get 'session/new'
  get 'sessions/new'
  get 'users/new'
  
  #ROOT
  root 'pages#home'
  get 'pages/comment'
  get 'pages/submit'
  get 'pages/login'
  get 'pages/register'
  get 'pages/about'
  
  get './signup',    to: 'user#create'
  
  get './login',     to: 'session#new'
  post './login',    to: 'session#create'
  delete './logout', to: 'session#destroy'
  
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end


